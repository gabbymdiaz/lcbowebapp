package sheridan;

import sheridan.LiquourType;

import java.util.ArrayList;
import java.util.List;

public class LiquourService {
	

    public List<String> getAvailableBrands(LiquourType type){

        List<String> brands = new ArrayList( );

        if(type.equals(LiquourType.WINE)){
            brands.add("Adrianna Vineyard");
            brands.add("J. P. Chenet");
            brands.add("Barefoot");
            brands.add("Spumante Bambino");
            brands.add("Yellowglen");

        }else if(type.equals(LiquourType.WHISKEY)){
            brands.add("Glenfiddich");
            brands.add("Johnnie Walker");
            brands.add("Jack Daniels");
            brands.add("Seagrams");
            brands.add("Buchanans");

        }else if(type.equals(LiquourType.BEER)){
            brands.add("Corona");
            brands.add("Budweiser");
            brands.add("Heineken");
            brands.add("Coors Light");
            brands.add("Canadian");

        }else {
            brands.add("No Brand Available");
        }
    return brands;
    }
}
